const express = require("express");
const app = express();
const server = require("http").Server(app);

const createDOMPurify = require("dompurify");
const { JSDOM } = require("jsdom");
const window = new JSDOM("").window;
const DOMPurify = createDOMPurify(window);
const sanitizeHtml = require("sanitize-html");

const io = require("socket.io")(server);

const router = require("./routes");

const path = require("path");

// Puerto en el que se ejecuta
const port = 3000;
app.set("port", process.env.PORT || port);

//Enlazamos con el router
app.use(router);

//Recursos estáticos
app.use("/", express.static(path.join(__dirname, "./public")));

//Sockets
io.on("connection", (socket) => {
  console.log("Cliente conectado");
  socket.emit("connectionOK", "Todo va bien");

  socket.on("newMessage", ({ name, message }) => {
    const nameClean = DOMPurify.sanitize(name);

    const messageClean = sanitizeHtml(DOMPurify.sanitize(message), {
      allowedTags: ["img"],
      allowedAttributes: {
        img: ["alt", "src", "width", "height"],
      },
    });

    io.emit("messageClient", { name: nameClean, message: messageClean });
  });
});

// Escuchar en el puerto que hemos definido
server.listen(app.get("port"), () => {
  console.log(`Server ready on port ${app.get("port")}`);
});
