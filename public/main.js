const socket = io.connect("/");

const messages = [];

socket.on("connectionOK", (message) => console.log(message));
socket.on("messageClient", (message) => {
  messages.push(
    `<div>
      <span>${message.name}</span>
      <span>${message.message}</span>
    </div>`
  );
  document.getElementById("messages").innerHTML = messages.join("");
});

//Función que envía un mensaje al servidor a través del socket
function addMessage(event) {
  event.preventDefault();
  const message = {
    name: event.target.name.value,
    message: event.target.message.value,
  };

  socket.emit("newMessage", message);
}
