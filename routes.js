const express = require("express");
const router = express.Router();
const path = require("path");

//MIDDLEWARE
router.use((req, res, next) => {
  console.log("Time", Date.now(), "Request", req.path);
  next();
});

//RUTA HOME
router.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "public/index.html"));
});

module.exports = router;
